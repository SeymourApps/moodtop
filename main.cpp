#include <windows.h>
#include <iostream>
#include <time.h>

#define HappyBTN 101
#define SadBTN 102
#define AngryBTN 103

class Mood {
public:
    char* name;
    std::string dir;

    HWND btn;
};

Mood happy;
Mood sad;
Mood angry;

time_t start = time(0);
double seconds;

std::string intList[5] = {"1", "2", "3", "4", "5"};

WNDCLASSEX window;

HWND wHwnd;

void SetPaper(std::string dir);
LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
    happy.name = "Happy";
    happy.dir = "\\Moods\\Happy\\";

    sad.name = "Sad";
    sad.dir = "\\Moods\\Sad\\";

    angry.name = "Angry";
    angry.dir = "\\Moods\\Angry\\";

    ZeroMemory(&window, sizeof(WNDCLASSEX));

    window.cbSize = sizeof(WNDCLASSEX);
    window.style = CS_HREDRAW | CS_VREDRAW;
    window.lpfnWndProc = WindowProc;
    window.hInstance = hInstance;
    window.hCursor = LoadCursor(NULL, IDC_ARROW);
    window.hbrBackground = (HBRUSH)COLOR_WINDOW;
    window.lpszClassName = "MainWindow";

    RegisterClassEx(&window);

    wHwnd = CreateWindowEx(NULL,
                           "MainWindow", "MoodTop",
                           WS_OVERLAPPEDWINDOW,
                           300, 300,
                           300, 300,
                           NULL, NULL,
                           hInstance, NULL);
    ShowWindow(wHwnd, nCmdShow);

    MSG msg;

    while(GetMessage(&msg, NULL, 0, 0)) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
    return msg.wParam;
}

LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
    switch(message) {
        case WM_CREATE: {
            happy.btn = CreateWindow("button", happy.name,
                                     WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
                                     10, 10,
                                     265, 70,
                                     hWnd, (HMENU)HappyBTN, NULL, NULL);
            sad.btn = CreateWindow("button", sad.name,
                                   WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
                                   10, 90,
                                   265, 70,
                                   hWnd, (HMENU)SadBTN, NULL, NULL);
            angry.btn = CreateWindow("button", angry.name,
                                     WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
                                     10, 170,
                                     265, 70,
                                     hWnd, (HMENU)AngryBTN, NULL, NULL);
        } break;
        case WM_COMMAND: {
            switch(LOWORD(wParam)) {
                case HappyBTN: {
                    SetPaper(happy.dir);
                } break;
                case SadBTN: {
                    SetPaper(sad.dir);
                } break;
                case AngryBTN: {
                    SetPaper(angry.dir);
                } break;
            }
        } break;
        case WM_DESTROY: {
            PostQuitMessage(0);
            return 0;
        } break;
    }

    return DefWindowProc(hWnd, message, wParam, lParam);
}

void SetPaper(std::string dir) {
    char currentDir[256];
    GetCurrentDirectory(256, currentDir);

    std::string img = dir + intList[rand() % 5] + (std::string)".jpg";
    std::string filepath = (std::string) currentDir + img;

    std::cout << filepath << std::endl;

    LPWSTR pic = (LPWSTR)filepath.c_str();

    int res;
    res = SystemParametersInfo(SPI_SETDESKWALLPAPER, 0, pic, SPIF_UPDATEINIFILE | SPIF_SENDCHANGE);
}
